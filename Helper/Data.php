<?php

namespace Smartmage\Shipping2Payment\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Data
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        LoggerInterface $logger
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
    }

    /**
     * @return array|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getShip2PayArray()
    {
        $ship2pay = unserialize($this->scopeConfig->getValue(
            'ship2pay/settings/ship2pay',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId())
        );

        return is_array($ship2pay) ? $ship2pay : [];
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
	public function isActive()
	{
		$ship2payActive = $this->scopeConfig->getValue(
			'ship2pay/settings/active',
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE,
			$this->storeManager->getStore()->getId());

		return $ship2payActive;
	}

    /**
     * @return LoggerInterface
     */
	public function getLogger()
    {
        return $this->logger;
    }

    public function getShip2Pay()
    {
        $ship2pay = unserialize($this->scopeConfig->getValue(
            'ship2pay/settings/ship2pay',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId())
        );

        return $ship2pay;
    }
}