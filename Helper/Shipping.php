<?php

namespace Smartmage\Shipping2Payment\Helper;

use Magento\Shipping\Model\Config\Source\Allmethods as Allmethods;

/**
 * Class Shipping
 */
class Shipping extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * All possible shipping method
     *
     * @var array
     */
    private $shippingMethod = [];

    /**
     * @var \Magento\Shipping\Model\Config\Source\Allmethods
     */
    private $allmethods;

    /**
     * @param Allmethods $allmethods
     */
    public function __construct(Allmethods $allmethods)
    {
        $this->allmethods = $allmethods;
    }

    /**
     * All possible shipping method
     *
     * @return array
     */
    public function getShippingMethods()
    {
        if (!$this->shippingMethod) {
            $this->shippingMethod = $this->allmethods->toOptionArray();
        }
        return $this->shippingMethod;
    }
}