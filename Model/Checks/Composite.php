<?php

namespace Smartmage\Shipping2Payment\Model\Checks;

use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Model\Quote;
use Smartmage\Shipping2Payment\Helper\Data AS HelperData;

class Composite extends \Magento\Payment\Model\Checks\Composite
{
    /**
     * @var \Magento\Payment\Model\Checks\SpecificationInterface[]
     */
    protected $list = [];

    /**
     * @var HelperData
     */
    protected $helper;

    /**
     * Composite constructor.
     *
     * @param HelperData $helper
     * @param array $list
     */
    public function __construct(HelperData $helper, array $list)
    {
        $this->list = $list;
        $this->helper = $helper;
    }

    /**
     * Check whether payment method is applicable to quote
     *
     * @param MethodInterface $paymentMethod
     * @param Quote $quote
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isApplicable(MethodInterface $paymentMethod, Quote $quote)
    {
        $result = parent::isApplicable($paymentMethod, $quote);

        if ($result && $this->getShip2PayHelper()->isActive()) {
            $ship2pay = $this->getShip2PayArray();
            $s2pCheck = false;
            if ($quote->getShippingAddress()->getShippingMethod() && is_array($ship2pay)){
                $shippingMethodCode = $quote->getShippingAddress()->getShippingMethod();
                $s2pCheck = true;
            }
            if ($s2pCheck && isset($ship2pay[$shippingMethodCode]) && is_array($ship2pay[$shippingMethodCode])) {
                if (in_array($paymentMethod->getCode(), $ship2pay[$shippingMethodCode])) {
                    $result = true;
                } else {
                    $result = false;
                }
            }
        }

    /**
     * @return \Smartmage\Shipping2Payment\Helper\Data
     */
        return $result;

    }

    /**
     * @return HelperData
     */
    public function getShip2PayHelper()
    {
        return $this->helper;
    }

    /**
     * @return array
     */
    public function getShip2PayArray()
    {
        $ship2payHelper = $this->getShip2PayHelper();
        $ship2pay = $ship2payHelper->getShip2PayArray();
        return $ship2pay;
    }

    public function getLogger()
    {
        return $this->helper->getLogger();
    }
}
