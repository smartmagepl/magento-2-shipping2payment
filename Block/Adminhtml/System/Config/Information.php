<?php
namespace Smartmage\Shipping2Payment\Block\Adminhtml\System\Config;

class Information extends \Magento\Config\Block\System\Config\Form\Field
{
	protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element){
		$html = '';

		$html .= '<div style="border:1px solid #CCCCCC; text-align: center; margin-top:10px; padding:10px 10px 0px 10px;"><p>' . __("Need a special feature, modification or module for Magento 2 ?<br/><br/> Contact us: <a href='mailto:biuro@smartmage.pl?Subject=Shipping2Payment'>biuro@smartmage.pl</a>") . '</p></div>';
		return $html;
	}
}