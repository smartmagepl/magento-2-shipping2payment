<?php

namespace Smartmage\Shipping2Payment\Block\Adminhtml\Form\Field;

use Smartmage\Shipping2Payment\Helper\Shipping;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

/**
 * Class Shipping
 */
class Shippings extends Select
{
    /**
     * @var Shipping
     */
    private $shippingHelper;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Shipping $shippingHelper
     * @param array $data
     */
    public function __construct(Context $context, Shipping $shippingHelper, array $data = [])
    {
        parent::__construct($context, $data);
        $this->shippingHelper = $shippingHelper;
    }
    
    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->shippingHelper->getShippingMethods());
        }
        return parent::_toHtml();
    }

    /**
     * Sets name for input element
     * 
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
